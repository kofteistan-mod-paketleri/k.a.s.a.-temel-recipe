import mods.gregtech.Assembler;

//dia wand

recipes.remove(<betterbuilderswands:wandDiamond>);

recipes.addShaped(<betterbuilderswands:wandDiamond>,
  [
    [     null      ,       null     , <ore:plateDiamond>],
    [     null      , <ore:stickWood>,       null      ],
    [<ore:stickWood>,       null     ,       null      ]
  ]
);

//unbreakbles

recipes.remove(<betterbuilderswands:wandUnbreakable:*>);

Assembler.addRecipe(<betterbuilderswands:wandUnbreakable:12>, [<gregtech:gt.metaitem.01:17506>,<ore:stickSteel>*2], null, 600, 128);

Assembler.addRecipe(<betterbuilderswands:wandUnbreakable:13>, [<betterbuilderswands:wandUnbreakable:12>,<betterbuilderswands:wandUnbreakable:12>], null, 1200, 512);

Assembler.addRecipe(<betterbuilderswands:wandUnbreakable:14>, [<betterbuilderswands:wandUnbreakable:13>,<betterbuilderswands:wandUnbreakable:13>], null, 2400, 2048);
