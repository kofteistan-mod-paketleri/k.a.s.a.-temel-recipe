//platformer

recipes.remove(<platforms:platform:50>);

recipes.addShaped(<platforms:platform:50>,
  [
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>],
    [<ore:dyeRed>, <minecraft:crafting_table:*>, <ore:dyeRed>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]
  ]
);

//wrench

recipes.remove(<platforms:wrench>);

recipes.addShaped(<platforms:wrench>,
  [
    [null, <ore:plateIron>, <ore:plateIron>],
    [<ore:craftingToolHardHammer>, <minecraft:iron_bars:*>, null],
    [<minecraft:iron_bars:*>, <ore:dyeRed>, null]
  ]
);
