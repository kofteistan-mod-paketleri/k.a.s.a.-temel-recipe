import mods.gregtech.CuttingSaw;
import mods.gregtech.PlateBender;
//obs plate

CuttingSaw.addRecipe([<IC2:itemPlates:7>*9], <minecraft:obsidian>, <liquid:water> * 104, 560, 30);

CuttingSaw.addRecipe([<IC2:itemPlates:7>*9], <minecraft:obsidian>, <liquid:ic2distilledwater> * 78, 560, 30);

CuttingSaw.addRecipe([<IC2:itemPlates:7>*9], <minecraft:obsidian>, <liquid:lubricant> * 26, 280, 30);

//dense obs plate

PlateBender.addRecipe(<IC2:itemDensePlates:7>,<ore:plateObsidian> * 9, 3600, 96);
