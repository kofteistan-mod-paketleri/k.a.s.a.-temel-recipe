import minetweaker.item.IItemStack;
import mods.gregtech.Assembler;

#####################################################################################################################################################################

var wooden_items = [<minecraft:wooden_pickaxe>,<minecraft:wooden_sword>,<minecraft:wooden_shovel>,<minecraft:wooden_axe>,<minecraft:wooden_hoe>] as IItemStack[];
for i, wooden in wooden_items{
  wooden.maxDamage = 1;
  recipes.remove(wooden);
  wooden.addTooltip(format.darkRed("Ha ha tahta eşyaları kullanamazsın!"));
}

#####################################################################################################################################################################

//recipe removes

#golden rail
recipes.remove(<minecraft:golden_rail>);

#quartz block
recipes.remove(<minecraft:quartz_block>);

#brick block
recipes.remove(<minecraft:brick_block>);

#rail
recipes.remove(<minecraft:rail>);

#golden rail
recipes.remove(<minecraft:golden_rail>);

#sandstone
recipes.remove(<minecraft:sandstone:*>);

#nether brick
recipes.remove(<minecraft:nether_brick>);

#golden apples
recipes.remove(<minecraft:golden_apple:*>);

#stone button
recipes.removeShapeless(<minecraft:stone_button>,
  [<ore:stone>,<ore:stone>]
);

#blaze powder
recipes.removeShapeless(<minecraft:blaze_powder>,
  [<minecraft:blaze_rod>]
);

#bone meal
recipes.removeShapeless(<minecraft:dye:15>, [<minecraft:bone>]);

#####################################################################################################################################################################

//recipes add

recipes.addShapeless(<minecraft:flint>, [<ore:gravel>,<ore:gravel>,<ore:gravel>]);

#jukebox

recipes.remove(<minecraft:jukebox>);

recipes.addShaped(<minecraft:jukebox>,
  [
    [<ore:plankWood>,  <ore:plankWood> ,<ore:plankWood>],
    [<ore:plankWood>,<ore:plateDiamond>,<ore:plankWood>],
    [<ore:plankWood>,  <ore:plankWood> ,<ore:plankWood>]
  ]
);

#note block

recipes.remove(<minecraft:noteblock>);

recipes.addShaped(<minecraft:noteblock>,
  [
    [<ore:plankWood>,  <ore:plankWood>  ,<ore:plankWood>],
    [<ore:plankWood>,<ore:plateRedstone>,<ore:plankWood>],
    [<ore:plankWood>,  <ore:plankWood>  ,<ore:plankWood>]
  ]
);

#redstone torch

recipes.remove(<minecraft:redstone_torch>);

recipes.addShaped(<minecraft:redstone_torch>,
  [
    [<ore:dustRedstone>],
    [ <minecraft:torch>]
  ]
);

#piston

recipes.remove(<minecraft:piston>);

recipes.addShaped(<minecraft:piston>,
  [
    [ <ore:plankWood> ,  <ore:plankWood>  , <ore:plankWood> ],
    [<ore:stoneCobble>, <ore:plateAnyIron>,<ore:stoneCobble>],
    [<ore:stoneCobble>,<ore:plateRedAlloy>,<ore:stoneCobble>]
  ]
);

#bookshelf

recipes.remove(<minecraft:bookshelf>);

recipes.addShaped(<minecraft:bookshelf>,
  [
    [<ore:plateWood> ,<ore:plateWood> ,<ore:plateWood> ],
    [<minecraft:book>,<minecraft:book>,<minecraft:book>],
    [<ore:plateWood> ,<ore:plateWood> ,<ore:plateWood> ]
  ]
);

#flint and steel

recipes.remove(<minecraft:flint_and_steel>);

recipes.addShaped(<minecraft:flint_and_steel>,
  [
    [<ore:ingotSteel>,       null      ],
    [      null      ,<minecraft:flint>]
  ]
);

#iron bars

recipes.remove(<minecraft:iron_bars>);

recipes.addShaped(<minecraft:iron_bars>*4,
  [
    [      null        ,<ore:craftingToolWrench>,       null       ],
    [<ore:stickAnyIron>,   <ore:stickAnyIron>   ,<ore:stickAnyIron>],
    [<ore:stickAnyIron>,   <ore:stickAnyIron>   ,<ore:stickAnyIron>]
  ]
);

#dispenser

recipes.remove(<minecraft:dispenser>);

recipes.addShaped(<minecraft:dispenser>,
  [
    [<ore:cobblestone>, <ore:cobblestone> ,<ore:cobblestone>],
    [<ore:cobblestone>,  <minecraft:bow>  ,<ore:cobblestone>],
    [<ore:cobblestone>,<ore:plateRedAlloy>,<ore:cobblestone>]
  ]
);

#chest

recipes.remove(<minecraft:chest>);

recipes.addShaped(<minecraft:chest>,
  [
    [<ore:plankWood>, <ore:plankWood> ,<ore:plankWood>],
    [<ore:plankWood>,<minecraft:flint>,<ore:plankWood>],
    [<ore:plankWood>, <ore:plankWood> ,<ore:plankWood>]
  ]
);

#furnace

recipes.remove(<minecraft:furnace>);

recipes.addShaped(<minecraft:furnace>,
  [
    [<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>],
    [<minecraft:flint>, <minecraft:flint>, <minecraft:flint>],
    [<ore:cobblestone>, <ore:cobblestone>, <ore:cobblestone>]
  ]
);

#fence

recipes.remove(<minecraft:fence>);

recipes.addShaped(<minecraft:fence>,
  [
    [<ore:stickWood>,   <ore:stickWood>   ,<ore:stickWood>],
    [<ore:stickWood>,   <ore:stickWood>   ,<ore:stickWood>],
    [     null      ,<ore:craftingToolSaw>,     null      ]
  ]
);

#wooden door

recipes.remove(<minecraft:wooden_door>);

recipes.addShaped(<minecraft:wooden_door>,
  [
    [<ore:plankWood>,       <ore:screwWood>       ,<ore:plankWood>],
    [<ore:plankWood>,<ore:craftingToolScrewdriver>,<ore:plankWood>],
    [<ore:plankWood>,       <ore:screwWood>       ,<ore:plankWood>]
  ]
);

#trapdoor

recipes.remove(<minecraft:trapdoor>);

recipes.addShaped(<minecraft:trapdoor>,
  [
    [<ore:plankWood>,       <ore:screwWood>       ,<ore:plankWood>],
    [<ore:screwWood>,<ore:craftingToolScrewdriver>,<ore:screwWood>],
    [<ore:plankWood>,       <ore:screwWood>       ,<ore:plankWood>]
  ]
);

#boat

recipes.remove(<minecraft:boat>);

recipes.addShaped(<minecraft:boat>,
  [
    [<ore:plateWood>,      null      ,<ore:plateWood>],
    [<ore:plateWood>, <ore:plateWood>,<ore:plateWood>]
  ]
);

#tripwire hook

recipes.remove(<minecraft:tripwire_hook>);

recipes.addShaped(<minecraft:tripwire_hook>,
  [
    [       <ore:ingotIron>      ],
    [       <ore:stickWood>      ],
    [<ore:craftingToolHardHammer>]
  ]
);

#bed

recipes.remove(<minecraft:bed>);

recipes.addShaped(<minecraft:bed>,
  [
    [<minecraft:wool:*>,<minecraft:wool:*>,<minecraft:wool:*>],
    [ <minecraft:fence>,  <ore:plankWood> , <minecraft:fence>]
  ]
);

//carpets

recipes.remove(<minecraft:carpet:*>);

for i in 0 to 16{
  recipes.addShaped(<minecraft:carpet>.definition.makeStack(i)*2,
    [
      [<minecraft:wool>.definition.makeStack(i),<ore:craftingToolSaw>]
    ]
  );
}

//glass panes

recipes.remove(<minecraft:stained_glass_pane:*>);

for i in 0 to 16{
  recipes.addShaped(<minecraft:stained_glass_pane>.definition.makeStack(i)*2,
    [
      [<minecraft:stained_glass>.definition.makeStack(i),<ore:craftingToolSaw>]
    ]
  );
}

recipes.remove(<minecraft:glass_pane>);

recipes.addShaped(<minecraft:glass_pane>*2,
  [
    [<minecraft:glass>,<ore:craftingToolSaw>]
  ]
);

//diamond tools

recipes.remove(<minecraft:diamond_sword>);

recipes.addShaped(<minecraft:diamond_sword>,
  [
    [<ore:plateDiamond>],
    [<ore:plateDiamond>],
    [ <ore:stickWood>  ]
  ]
);

recipes.remove(<minecraft:diamond_shovel>);

recipes.addShaped(<minecraft:diamond_shovel>,
  [
    [<ore:plateDiamond>],
    [ <ore:stickWood>  ],
    [ <ore:stickWood>  ]
  ]
);

recipes.remove(<minecraft:diamond_pickaxe>);

recipes.addShaped(<minecraft:diamond_pickaxe>,
  [
    [<ore:plateDiamond>, <ore:plateDiamond>, <ore:plateDiamond>],
    [       null       ,   <ore:stickWood> ,        null       ],
    [       null       ,   <ore:stickWood> ,        null       ]
  ]
);

recipes.remove(<minecraft:diamond_axe>);

recipes.addShaped(<minecraft:diamond_axe>,
  [
    [<ore:plateDiamond>,<ore:plateDiamond>],
    [<ore:plateDiamond>,  <ore:stickWood> ],
    [       null       ,  <ore:stickWood> ]
  ]
);

recipes.remove(<minecraft:diamond_hoe>);

recipes.addShaped(<minecraft:diamond_hoe>,
  [
    [<ore:plateDiamond>,<ore:plateDiamond>],
    [       null       , <ore:stickWood>  ],
    [       null       , <ore:stickWood>  ]
  ]
);

//diamond zırh

recipes.remove(<minecraft:diamond_helmet>);

recipes.addShaped(<minecraft:diamond_helmet>,
  [
    [<ore:plateDiamond>,    <ore:plateDiamond>       ,<ore:plateDiamond>],
    [<ore:plateDiamond>,<ore:craftingToolHardHammer> ,<ore:plateDiamond>]
  ]
);

recipes.remove(<minecraft:diamond_chestplate>);

recipes.addShaped(<minecraft:diamond_chestplate>,
  [
    [<ore:plateDiamond>,<ore:craftingToolHardHammer>,<ore:plateDiamond>],
    [<ore:plateDiamond>,     <ore:plateDiamond>     ,<ore:plateDiamond>],
    [<ore:plateDiamond>,     <ore:plateDiamond>     ,<ore:plateDiamond>]
  ]
);

recipes.remove(<minecraft:diamond_leggings>);

recipes.addShaped(<minecraft:diamond_leggings>,
  [
    [<ore:plateDiamond>,    <ore:plateDiamond>      ,<ore:plateDiamond>],
    [<ore:plateDiamond>,<ore:craftingToolHardHammer>,<ore:plateDiamond>],
    [<ore:plateDiamond>,            null            ,<ore:plateDiamond>]
  ]
);

recipes.remove(<minecraft:diamond_boots>);

recipes.addShaped(<minecraft:diamond_boots>,
  [
    [<ore:plateDiamond>,<ore:craftingToolHardHammer>,<ore:plateDiamond>],
    [<ore:plateDiamond>,           null             ,<ore:plateDiamond>]
  ]
);
