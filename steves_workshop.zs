import mods.gregtech.Assembler;
import minetweaker.item.IItemStack;

//removes
val delete = [ <StevesWorkshop:production_table_upgrade:2>,<StevesWorkshop:production_table>,<StevesWorkshop:production_table_upgrade:7>,<StevesWorkshop:production_table_upgrade>,<StevesWorkshop:production_table_upgrade:10>,<StevesWorkshop:production_table_upgrade:9>,<StevesWorkshop:production_table_upgrade:11>,<StevesWorkshop:production_table_upgrade:1>,<StevesWorkshop:production_table_upgrade:8>,<StevesWorkshop:production_table_upgrade:4>,<StevesWorkshop:production_table_upgrade:6>,<StevesWorkshop:production_table_upgrade:5>,<StevesWorkshop:production_table_upgrade:3>] as IItemStack[];
for i, del in delete{
  recipes.remove(del);
}

//add
recipes.addShaped(<StevesWorkshop:production_table_upgrade>,
  [
    [<ore:stone>, <ore:plankWood>,<ore:stone>    ],
    [<ore:plankWood>, <ore:stone>,<ore:plankWood>],
    [<ore:stone>, <ore:plankWood>,<ore:stone>    ]
  ]
);

//sandık
recipes.addShaped(<StevesWorkshop:production_table_upgrade:2>,
  [
    [<minecraft:chest:*>                      ,<StevesWorkshop:production_table_upgrade>,<minecraft:chest:*>                      ],
    [<StevesWorkshop:production_table_upgrade>,<minecraft:chest:*>                      ,<StevesWorkshop:production_table_upgrade>],
    [<minecraft:chest:*>                      ,<StevesWorkshop:production_table_upgrade>,<minecraft:chest:*>                      ]
  ]
);

//lav geliştirmesi
recipes.addShaped(<StevesWorkshop:production_table_upgrade:7>,
  [
    [<minecraft:lava_bucket>*1  ,<IC2:blockMachine:1>                     ,<minecraft:lava_bucket>*1  ],
    [<Natura:NetherFurnace>     ,<StevesWorkshop:production_table_upgrade>,<Natura:NetherFurnace>     ],
    [<Railcraft:machine.alpha:5>,<minecraft:netherrack:*>                 ,<Railcraft:machine.alpha:5>]
  ]
);

//table
recipes.addShaped(<StevesWorkshop:production_table>,
  [
    [<minecraft:chest>         ,<minecraft:chest>                        ,<minecraft:chest>         ],
    [<minecraft:furnace>       ,<StevesWorkshop:production_table_upgrade>,<minecraft:crafting_table>],
    [<minecraft:crafting_table>,<minecraft:chest>                        ,<minecraft:furnace>       ]
  ]
);

<StevesWorkshop:production_table>.displayName = "Production Table";
