// Importing stuffs

import mods.gregtech.Assembler;
import minetweaker.item.IItemStack;
import minetweaker.item.IIngredient;
import mods.gregtech.ForgeHammer;
import mods.gregtech.BlastFurnace;
import mods.gregtech.Mixer;
import mods.gregtech.AlloySmelter;

// Valuables

val Pcb = <StevesCarts:ModuleComponents:9>;
val A_Pcb = <StevesCarts:ModuleComponents:16>;
val CPane = <StevesCarts:ModuleComponents:30>;
val LCPane = <StevesCarts:ModuleComponents:31>;
val HCPane = <StevesCarts:ModuleComponents:32>;
val IPane = <StevesCarts:ModuleComponents:34>;
val LIPane = <StevesCarts:ModuleComponents:35>;
val HIPane = <StevesCarts:ModuleComponents:36>;
val DPane = <StevesCarts:ModuleComponents:37>;
val LDPane = <StevesCarts:ModuleComponents:38>;
val HDPane = <StevesCarts:ModuleComponents:39>;
val BSaw = <StevesCarts:ModuleComponents:15>;



val BCircuit = <IC2:itemPartCircuit>;
val PIron = <ore:plateAnyIron>;
val PWood = <ore:plateWood>;
val PRedAlloy = <ore:plateRedAlloy>;
val IScrew = <ore:screwAnyIron>;
val LVmotor = <gregtech:gt.metaitem.01:32600>;
val GIron = <ore:gearIron>;
val PSteel = <ore:plateSteel>;

####################################################
val Saw = <ore:craftingToolSaw>;
val SHammer = <ore:craftingToolSoftHammer>;
val Hammer = <ore:craftingToolHardHammer>;
val File = <ore:craftingToolFile>;
val Screwdriver = <ore:craftingToolScrewdriver>;
val Wrench = <ore:craftingToolWrench>;
####################################################

val Planks = <ore:plankWood>;
val Wood = <ore:logWood>;
val Stone = <ore:stoneSmooth>;
val Diamond = <ore:gemDiamond>;



var delete = [<StevesCarts:upgrade>, <StevesCarts:upgrade:1>,<StevesCarts:upgrade:2>,<StevesCarts:upgrade:3>,<StevesCarts:upgrade:4>,<StevesCarts:upgrade:5>,<StevesCarts:upgrade:6>,<StevesCarts:upgrade:7>,<StevesCarts:upgrade:8>,<StevesCarts:upgrade:9>,<StevesCarts:upgrade:10>,<StevesCarts:upgrade:13>,<StevesCarts:upgrade:14>,<StevesCarts:upgrade:15>,<StevesCarts:upgrade:16>,<StevesCarts:upgrade:17>,<StevesCarts:upgrade:18>,<StevesCarts:upgrade:19>] as IItemStack[];
for i, del in delete{
recipes.remove(del);
del.addTooltip(format.darkRed("This item is disabled!"));
}

recipes.remove(<StevesCarts:CartModule:62>);
<StevesCarts:CartModule:62>.addTooltip(format.darkRed("Why would you want to use this item?"));






















//Simple pcb

recipes.remove(<StevesCarts:ModuleComponents:9>);
recipes.addShaped(<StevesCarts:ModuleComponents:9>,
[
 [<ore:plateAnyIron>, <ore:plateRedAlloy>, <ore:plateAnyIron>],
  [<ore:plateRedAlloy>, <ore:circuitBasic>, <ore:plateRedAlloy>],
  [<ore:plateAnyIron>, <ore:plateRedAlloy>, <ore:plateAnyIron>]
]);

//Advanced Pcb

recipes.remove(<StevesCarts:ModuleComponents:16>);
recipes.addShaped(<StevesCarts:ModuleComponents:16>,
[
  [PRedAlloy, PIron, PRedAlloy],
  [Pcb, PIron, Pcb],
  [PRedAlloy, PIron, PRedAlloy]
]);

//Cart Assembler

recipes.remove(<StevesCarts:BlockCartAssembler>);

recipes.addShaped(<StevesCarts:BlockCartAssembler>,
[[<ore:plateAnyIron>, <ore:stone>, <ore:plateAnyIron>],
[<ore:stone>, <ore:plateIron>, <ore:stone>],
[<StevesCarts:ModuleComponents:9>, <ore:stone>, <StevesCarts:ModuleComponents:9>]]);

//Wooden wheel

recipes.remove(<StevesCarts:ModuleComponents>);

recipes.addShaped(<StevesCarts:ModuleComponents>,
  [
    [<ore:craftingToolScrewdriver>, <gregtech:gt.metaitem.02:32470>, <ore:craftingToolSaw>],
    [<ore:screwWood>, <ore:stickLongWood>, <ore:screwWood>],
    [<gregtech:gt.metaitem.02:31809>, <gregtech:gt.metaitem.02:32470>, <gregtech:gt.metaitem.02:31809>]
  ]
);

//Wooden hull

recipes.remove(<StevesCarts:CartModule:37>);

recipes.addShaped(<StevesCarts:CartModule:37>,
  [
    [<gregtech:gt.metaitem.01:17809>, <ore:craftingToolSaw>, <gregtech:gt.metaitem.01:17809>],
    [<gregtech:gt.metaitem.01:17809>, <gregtech:gt.metaitem.01:17809>, <gregtech:gt.metaitem.01:17809>],
    [<StevesCarts:ModuleComponents>, <ore:craftingToolFile>, <StevesCarts:ModuleComponents>]
  ]
);

//Iron Wheel

recipes.remove(<StevesCarts:ModuleComponents:1>);
recipes.addShaped(<StevesCarts:ModuleComponents:1>,
  [
    [<ore:craftingToolScrewdriver>, <ore:plateIron>, <ore:craftingToolHardHammer>],
    [<ore:screwAnyIron>, <ore:stickLongAnyIron>, <ore:screwAnyIron>],
    [<ore:gearIron>, <ore:plateIron>, <ore:gearIron>]
  ]
);

//Milker
recipes.remove(<StevesCarts:CartModule:86>);
recipes.addShaped(<StevesCarts:CartModule:86>,
[
  [<minecraft:wheat>,<minecraft:wheat>,<minecraft:wheat>],
  [Pcb, <minecraft:milk_bucket>, Pcb],
  [Wrench, A_Pcb, Hammer],
]);

//Iron Hull

recipes.remove(<StevesCarts:CartModule:38>);

recipes.addShaped(<StevesCarts:CartModule:38>,
  [
    [<ore:plateIron>, <ore:craftingToolHardHammer>, <ore:plateIron>],
    [<ore:plateIron>, <ore:plateIron>, <ore:plateIron>],
    [<StevesCarts:ModuleComponents:1>, <ore:craftingToolFile>, <StevesCarts:ModuleComponents:1>]
  ]
);

//Red Pigment
recipes.remove(<StevesCarts:ModuleComponents:2>);
recipes.addShaped(<StevesCarts:ModuleComponents:2>,
[
  [<minecraft:dye:1>, <minecraft:dye:1>, <minecraft:dye:1>],
  [<minecraft:glowstone_dust>, <minecraft:daylight_detector>,<minecraft:glowstone_dust>],
  [<minecraft:dye:1>, <minecraft:dye:1>, <minecraft:dye:1>],
]);

//Green Pigment
recipes.remove(<StevesCarts:ModuleComponents:3>);
recipes.addShaped(<StevesCarts:ModuleComponents:3>,
[
  [<ore:dyeGreen>,<ore:dyeGreen>,<ore:dyeGreen>],
  [<minecraft:glowstone_dust>,<minecraft:daylight_detector>,<minecraft:glowstone_dust>],
  [<ore:dyeGreen>,<ore:dyeGreen>,<ore:dyeGreen>],
]);

//Blue Pigment
recipes.remove(<StevesCarts:ModuleComponents:4>);
recipes.addShaped(<StevesCarts:ModuleComponents:4>,
[
  [<ore:dyeBlue>,<ore:dyeBlue>,<ore:dyeBlue>],
  [<minecraft:glowstone_dust>, <minecraft:daylight_detector>,<minecraft:glowstone_dust>],
  [<ore:dyeBlue>,<ore:dyeBlue>,<ore:dyeBlue>],
]);

//Fuse
recipes.remove(<StevesCarts:ModuleComponents:43>);
recipes.addShaped(<StevesCarts:ModuleComponents:43>*3,
[
  [null,<minecraft:string>,null],
  [<minecraft:string>,<ProjRed|Core:projectred.core.part:35>,<minecraft:string>],
  [null,<minecraft:string>,null],
]);

//Dynamite
recipes.remove(<StevesCarts:ModuleComponents:6>);
recipes.addShapeless(<StevesCarts:ModuleComponents:6>*2, [<IC2:itemDynamite>, <StevesCarts:ModuleComponents:43>,<StevesCarts:ModuleComponents:43>]);

//Height Controller
recipes.remove(<StevesCarts:CartModule:19>);
recipes.addShaped(<StevesCarts:CartModule:19>,
[
  [PIron,<minecraft:compass>,PIron],
  [PIron,A_Pcb,PIron],
  [PIron,<minecraft:paper>,PIron],
]);

//Torch Placer
recipes.remove(<StevesCarts:CartModule:7>);
recipes.addShaped(<StevesCarts:CartModule:7>,
[
  [<StevesCarts:ModuleComponents:29>,null,<StevesCarts:ModuleComponents:29>],
  [PIron,<minecraft:glowstone>,PIron],
  [PIron,PIron,PIron],
]);

//Empty Disk
recipes.remove(<StevesCarts:ModuleComponents:28>);
recipes.addShaped(<StevesCarts:ModuleComponents:28>,
[
  [<minecraft:paper>,PRedAlloy,<minecraft:paper>],
  [<minecraft:paper>,Pcb,<minecraft:paper>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Entity Detector: Animal
recipes.remove(<StevesCarts:CartModule:21>);
recipes.addShaped(<StevesCarts:CartModule:21>,
[
  [PRedAlloy, <minecraft:porkchop>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Entity Detector: Villager
recipes.remove(<StevesCarts:CartModule:23>);
recipes.addShaped(<StevesCarts:CartModule:23>,
[
  [PRedAlloy, <minecraft:emerald>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Entity Detector: Player
recipes.remove(<StevesCarts:CartModule:22>);
recipes.addShaped(<StevesCarts:CartModule:22>,
[
  [PRedAlloy, <minecraft:diamond>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Entity Detector: Monster
recipes.remove(<StevesCarts:CartModule:24>);
recipes.addShaped(<StevesCarts:CartModule:24>,
[
  [PRedAlloy, <minecraft:rotten_flesh>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Entity Detector: Projectile
recipes.remove(<StevesCarts:CartModule:51>);
recipes.addShaped(<StevesCarts:CartModule:51>,
[
  [PRedAlloy, <minecraft:glass_bottle>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Projectile:Egg
recipes.remove(<StevesCarts:CartModule:53>);
recipes.addShaped(<StevesCarts:CartModule:53>,
[
  [PRedAlloy,<minecraft:egg>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Crop: Nether Wart
recipes.remove(<StevesCarts:CartModule:58>);
recipes.addShaped(<StevesCarts:CartModule:58>,
[
  [PRedAlloy, <minecraft:nether_wart>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [<minecraft:paper>,<minecraft:paper>,<minecraft:paper>],
]);

//Tree:Exotic
recipes.remove(<StevesCarts:CartModule:88>);
recipes.addShaped(<StevesCarts:CartModule:88>,
[
  [PRedAlloy, <ore:treeSapling>, PRedAlloy],
  [<minecraft:wool:15>,<StevesCarts:ModuleComponents:28>,<minecraft:wool:15>],
  [Pcb,<minecraft:paper>,Pcb],
]);

//Colorizer
recipes.remove(<StevesCarts:CartModule:41>);
recipes.addShaped(<StevesCarts:CartModule:41>,
[
  [<StevesCarts:ModuleComponents:2>,<StevesCarts:ModuleComponents:3>,<StevesCarts:ModuleComponents:4>],
  [PIron,PIron,PIron],
  [null,PIron,null],
]);

//Wood Cutting Core

recipes.remove(<StevesCarts:ModuleComponents:17>);

recipes.addShaped(<StevesCarts:ModuleComponents:17>,
[
  [<IC2:itemHarz>, <IC2:itemHarz>, <IC2:itemHarz>],
  [<IC2:itemHarz>, A_Pcb, <IC2:itemHarz>],
  [<IC2:itemHarz>, <IC2:itemHarz>, <IC2:itemHarz>],
]);

//Crafter
recipes.remove(<StevesCarts:CartModule:87>);
recipes.addShaped(<StevesCarts:CartModule:87>,
[
  [Hammer, A_Pcb, Wrench],
  [PIron, <BuildCraft|Factory:autoWorkbenchBlock>, PIron],
  [Pcb, PIron, Pcb],
]);

//Smelter
recipes.remove(<StevesCarts:CartModule:91>);
recipes.addShaped(<StevesCarts:CartModule:91>,
[
  [Hammer, A_Pcb, Wrench],
  [PIron, <IC2:blockMachine:1>, PIron],
  [Pcb, PIron, Pcb],
]);

//Advanced Crafter
recipes.remove(<StevesCarts:CartModule:92>);
recipes.addShaped(<StevesCarts:CartModule:92>,
[
  [PSteel, A_Pcb, PSteel],
  [Pcb, <StevesCarts:CartModule:87>, Pcb],
  [PSteel, Diamond, PSteel],
]);

//Advanced Smelter
recipes.remove(<StevesCarts:CartModule:93>);
recipes.addShaped(<StevesCarts:CartModule:93>,
[
  [PSteel, A_Pcb, PSteel],
  [Pcb, <StevesCarts:CartModule:91>, Pcb],
  [PSteel, Diamond, PSteel],
]);

//Basic Wood Cutter

recipes.remove(<StevesCarts:CartModule:15>.withTag({Data: 100 as byte}));

recipes.addShaped(<StevesCarts:CartModule:15>.withTag({Data: 100 as byte}),
[
  [BSaw, BSaw, BSaw,],
  [BSaw, <StevesCarts:ModuleComponents:17>, BSaw],
  [Hammer, LVmotor, Wrench],
]);

//Drill Intelligence
recipes.remove(<StevesCarts:CartModule:75>);
recipes.addShaped(<StevesCarts:CartModule:75>,
[
  [<ore:plateGold>,<ore:plateGold>,<ore:plateGold>],
  [PIron, A_Pcb, PIron],
  [A_Pcb, Hammer, A_Pcb],
]);

//Power Observer
recipes.remove(<StevesCarts:CartModule:77>);
recipes.addShaped(<StevesCarts:CartModule:77>,
[
  [Hammer, <gregtech:gt.metaitem.01:32640>, Wrench],
  [PIron, PRedAlloy,PIron],
  [PRedAlloy,A_Pcb,PRedAlloy],
]);

//Seat
recipes.remove(<StevesCarts:CartModule:25>);
recipes.addShaped(<StevesCarts:CartModule:25>,
[
  [<gregtech:gt.metaitem.01:27032>,Planks,<gregtech:gt.metaitem.01:27032>],
  [SHammer, Planks, Wrench],
  [<ore:slabWood>,<ore:slabWood>,<ore:slabWood>],
]);

//Wheel
recipes.remove(<StevesCarts:ModuleComponents:14>);
recipes.addShaped(<StevesCarts:ModuleComponents:14>,
[
  [PIron, <ore:stickWood>, PIron],
  [<ore:stickWood>, IScrew, <ore:stickWood>],
  [SHammer, <ore:stickWood>, Wrench],
]);

//

//Diamond Saw

recipes.remove(<StevesCarts:ModuleComponents:15>);

recipes.addShaped(<StevesCarts:ModuleComponents:15>,
[
  [IScrew, IScrew, Hammer],
  [PIron, PIron, <ore:gemDiamond>],
  [IScrew, IScrew, Wrench],
]);

//Basic farmer
recipes.remove(<StevesCarts:CartModule:14>);
recipes.addShaped(<StevesCarts:CartModule:14>,
[
  [Diamond, Diamond, Diamond],
  [Pcb, LVmotor, Diamond],
  [<ore:stickLongSteel>, Pcb, Diamond],
]);

//Fertilezer
recipes.remove(<StevesCarts:CartModule:18>);
recipes.addShaped(<StevesCarts:CartModule:18>,
[
  [<Forestry:fertilizerCompound>, <minecraft:milk_bucket>, <Forestry:fertilizerCompound>],
  [<Railcraft:fluid.creosote.bottle>,<Backpack:tannedLeather>,<Railcraft:fluid.creosote.bottle>],
  [<Backpack:tannedLeather>,<StevesCarts:ModuleComponents:9>,<Backpack:tannedLeather>],
]);

//Hydrator
recipes.remove(<StevesCarts:CartModule:16>);
recipes.addShaped(<StevesCarts:CartModule:16>,
[
  [PIron, <minecraft:potion>, PIron],
  [PIron,<minecraft:fence>, PIron],
  [<minecraft:fence>, Wrench, <minecraft:fence>],
]);

//Coal engine

recipes.remove(<StevesCarts:CartModule>);
recipes.addShaped(<StevesCarts:CartModule>,
 [
	[<ore:plateDoubleIron>, <gregtech:gt.blockcasings:1>, <ore:plateDoubleIron>],
	[<gregtech:gt.metaitem.01:32640>, <IC2:blockMachine:1> ,<gregtech:gt.metaitem.01:32640>],
	[<gregtech:gt.metaitem.01:27305>, <ore:craftingToolWrench>, <gregtech:gt.metaitem.02:20305>],
 ]
);

//Tiny Coal Engine

recipes.remove(<StevesCarts:CartModule:44>);

recipes.addShaped(<StevesCarts:CartModule:44>,
[
	[<ore:plateDoubleIron>, <IC2:blockMachine:1>, <ore:plateDoubleIron>],
	[<ore:plateDoubleIron>, <ore:gearIron>, <ore:plateDoubleIron>],
	[<gregtech:gt.metaitem.01:27032>, <ore:craftingToolWrench>, <gregtech:gt.metaitem.01:32600>],
]);

//Railer
recipes.remove(<StevesCarts:CartModule:10>);
recipes.addShaped(<StevesCarts:CartModule:10>,
[
  [<ore:gearStone>, <ore:gearStone>, <ore:gearStone>],
  [PIron, <minecraft:rail>, PIron],
  [<ore:gearStone>, <ore:gearStone>, <ore:gearStone>,],
]);

//Advanced Railer
recipes.remove(<StevesCarts:CartModule:11>);
recipes.addShaped(<StevesCarts:CartModule:11>,
[
  [PIron, PIron, PIron],
  [<StevesCarts:CartModule:10>, <minecraft:rail>, <StevesCarts:CartModule:10>],
  [PIron, PIron, PIron],
]);

//Bridge Builder
recipes.remove(<StevesCarts:CartModule:12>);
recipes.addShaped(<StevesCarts:CartModule:12>,
[
  [<minecraft:nether_brick>, PRedAlloy, <minecraft:nether_brick>],
  [<minecraft:nether_brick>, Pcb, <minecraft:nether_brick>],
  [Hammer, <gregtech:gt.metaitem.01:32640>, Wrench],
]);

//Iron Drill

recipes.remove(<StevesCarts:CartModule:42>);

recipes.addShaped(<StevesCarts:CartModule:42>,
[
 [<IC2:blockFenceIron>, IScrew, Screwdriver],
 [GIron, PIron, <IC2:itemIronBlockCuttingBlade>],
 [<IC2:blockFenceIron>, IScrew, Hammer],
]
);

//Diamond Drill

recipes.remove(<StevesCarts:CartModule:8>);

recipes.addShaped(<StevesCarts:CartModule:8>,
[
  [PIron, IScrew, Screwdriver],
  [<StevesCarts:CartModule:42>, PIron, <IC2:itemDiamondBlockCuttingBlade>],
  [PIron, IScrew, Hammer],
]);

//Reinforced Wheels
recipes.remove(<StevesCarts:ModuleComponents:23>);
recipes.addShaped(<StevesCarts:ModuleComponents:23>,
[
  [Hammer, <StevesCarts:ModuleComponents:22>, Wrench],
  [<StevesCarts:ModuleComponents:22>, <ore:screwSteel>, <StevesCarts:ModuleComponents:22>],
  [<ore:gearSteel>, <StevesCarts:ModuleComponents:22>,<ore:gearSteel>],
]);

//Tank Valve

recipes.remove(<StevesCarts:ModuleComponents:60>);
recipes.addShaped(<StevesCarts:ModuleComponents:60>*6 ,
[
  [null, PIron, null],
  [PIron, Hammer, PIron],
  [PIron, <minecraft:iron_bars>,PIron],
]);

//Liquid Manager
recipes.remove(<StevesCarts:BlockLiquidManager>);
recipes.addShaped(<StevesCarts:BlockLiquidManager>,
[
  [<StevesCarts:CartModule:66>, PIron, <StevesCarts:CartModule:66>],
  [PIron, Wrench, PIron],
  [<StevesCarts:CartModule:66>, PIron, <StevesCarts:CartModule:66>],
]);

//Melter
recipes.remove(<StevesCarts:CartModule:33>);
recipes.addShaped(<StevesCarts:CartModule:33>,
[
  [<gregtech:gt.blockmachines:4401>, <minecraft:glowstone>, <gregtech:gt.blockmachines:4401>],
  [<minecraft:glowstone>, <gregtech:gt.blockmachines:104>, <minecraft:glowstone>],
  [<gregtech:gt.blockmachines:4401>, <minecraft:glowstone>, <gregtech:gt.blockmachines:4401>],
]);

//External Distrubator
recipes.remove(<StevesCarts:BlockDistributor>);
recipes.addShaped(<StevesCarts:BlockDistributor>,
[
  [<minecraft:stone>, Pcb,<minecraft:stone>],
  [Pcb, PRedAlloy,Pcb],
  [<minecraft:stone>, Pcb,<minecraft:stone>],
]);

//Module Toggler
recipes.remove(<StevesCarts:BlockActivator>);
recipes.addShaped(<StevesCarts:BlockActivator>,
[
  [<ore:dyeOrange>, <ore:plateGold>, <ore:gemLapis>],
  [<ore:stone>, <ore:plateAnyIron>, <ore:stone>],
  [PRedAlloy, <StevesCarts:ModuleComponents:16>, PRedAlloy],
]);

//Detector Unit
recipes.remove(<StevesCarts:BlockDetector:1>);
recipes.addShaped(<StevesCarts:BlockDetector:1>,
[
  [<minecraft:stonebrick>, <minecraft:stone_pressure_plate>, <minecraft:stonebrick>],
  [Pcb, Wrench, Pcb],
  [<minecraft:stonebrick>, PRedAlloy, <minecraft:stonebrick>],
]);

//Detector Redstone Unit
recipes.remove(<StevesCarts:BlockDetector:4>);
recipes.addShaped(<StevesCarts:BlockDetector:4>,
[
  [PRedAlloy, PIron, PRedAlloy],
  [PIron, <StevesCarts:BlockDetector:1>, PIron],
  [PRedAlloy, Hammer, PRedAlloy],
]);

//Shooting Station
recipes.remove(<StevesCarts:ModuleComponents:25>);
recipes.addShaped(<StevesCarts:ModuleComponents:25>,
[
  [PRedAlloy,Hammer, PRedAlloy],
  [PRedAlloy,<gregtech:gt.metaitem.01:17086>,PRedAlloy],
  [<minecraft:dispenser>,Pcb,<minecraft:dispenser>],
]);

//Entity Scanner
recipes.remove(<StevesCarts:ModuleComponents:26>);
recipes.addShaped(<StevesCarts:ModuleComponents:26>,
[
  [<gregtech:gt.metaitem.01:17086>,A_Pcb,<gregtech:gt.metaitem.01:17086>],
  [PRedAlloy, A_Pcb, PRedAlloy],
  [PRedAlloy,Wrench,PRedAlloy],
]);

//Entity Analyzer
recipes.remove(<StevesCarts:ModuleComponents:27>);
recipes.addShaped(<StevesCarts:ModuleComponents:27>,
[
  [PIron,Wrench,PIron],
  [PIron,A_Pcb,PIron],
  [PIron,PIron,PIron],
]);

//Cleaning Fan
recipes.remove(<StevesCarts:ModuleComponents:40>);
recipes.addShaped(<StevesCarts:ModuleComponents:40>,
[
  [<minecraft:iron_bars>,PRedAlloy,<minecraft:iron_bars>],
  [PRedAlloy,Hammer,PRedAlloy],
  [<minecraft:iron_bars>,PRedAlloy,<minecraft:iron_bars>],
]);

//Cleaning Tube
recipes.remove(<StevesCarts:ModuleComponents:42>);
recipes.addShaped(<StevesCarts:ModuleComponents:42>,
[
  [<minecraft:dye:14>,PIron,<minecraft:dye:14>],
  [<minecraft:dye:14>,<gregtech:gt.metaitem.03:32012>,<minecraft:dye:14>],
  [<minecraft:dye:14>,PIron,<minecraft:dye:14>],
]);

//Cleaning Core
recipes.remove(<StevesCarts:ModuleComponents:41>);
recipes.addShaped(<StevesCarts:ModuleComponents:41>,
[
  [<StevesCarts:ModuleComponents:40>,Wrench,<StevesCarts:ModuleComponents:40>],
  [<StevesCarts:ModuleComponents:42>,Pcb,<StevesCarts:ModuleComponents:42>],
  [PIron,<StevesCarts:ModuleComponents:42>,PIron],
]);

//Liquid Cleaning Tube
recipes.remove(<StevesCarts:ModuleComponents:65>);
recipes.addShaped(<StevesCarts:ModuleComponents:65>,
[
  [<ore:dyeGreen>,PIron,<ore:dyeGreen>],
  [<ore:dyeGreen>,<gregtech:gt.metaitem.03:32012>,<ore:dyeGreen>],
  [<ore:dyeGreen>,PIron,<ore:dyeGreen>],
]);

//Liquid Cleaning Core
recipes.remove(<StevesCarts:ModuleComponents:64>);
recipes.addShaped(<StevesCarts:ModuleComponents:64>,
[
  [<StevesCarts:ModuleComponents:40>,Wrench,<StevesCarts:ModuleComponents:40>],
  [<StevesCarts:ModuleComponents:65>,Pcb,<StevesCarts:ModuleComponents:65>],
  [PIron,<StevesCarts:ModuleComponents:65>,PIron],
]);

//Cage
recipes.remove(<StevesCarts:CartModule:57>);
recipes.addShaped(<StevesCarts:CartModule:57>,
[
  [<minecraft:fence>,<minecraft:fence>,<minecraft:fence>],
  [<gregtech:gt.metaitem.01:27809>,Pcb,<gregtech:gt.metaitem.01:27809>],
  [<minecraft:fence>,<minecraft:fence>,<minecraft:fence>],
]);

//Detector Manager
recipes.remove(<StevesCarts:BlockDetector>);
recipes.addShaped(<StevesCarts:BlockDetector>,
[
  [Stone, Stone, Stone],
  [PIron, PIron, PIron],
  [Hammer, <StevesCarts:BlockDetector:1>, Wrench],
]);

//Detector Junction
recipes.remove(<StevesCarts:BlockDetector:3>);
recipes.addShaped(<StevesCarts:BlockDetector:3>,
[
  [<StevesCarts:CartModule:7>, Stone, <StevesCarts:CartModule:7>],
  [PRedAlloy, <StevesCarts:BlockDetector:1>, PRedAlloy],
  [PIron, Pcb, PIron],
]);

//Detector Station
recipes.remove(<StevesCarts:BlockDetector:2>);
recipes.addShaped(<StevesCarts:BlockDetector:2>,
[
  [PIron, PIron, PIron],
  [PIron, <StevesCarts:BlockDetector:1>, PIron],
  [Pcb, Wrench, Pcb],
]);

//Panes
  recipes.remove(<StevesCarts:ModuleComponents:30>);
  recipes.addShaped(<StevesCarts:ModuleComponents:30>*8, [
  [Planks, Planks, Planks],
  [Saw, Wood, SHammer],
  [Planks, Planks, Planks]]);

  recipes.remove(<StevesCarts:ModuleComponents:31>);
  recipes.addShaped(<StevesCarts:ModuleComponents:31>, [
  [CPane, CPane, CPane],
  [CPane, Saw, CPane],
  [CPane, CPane, CPane]]);

  recipes.remove(<StevesCarts:ModuleComponents:32>);
  recipes.addShaped(<StevesCarts:ModuleComponents:32>, [
  [PWood, LCPane,PWood],
  [LCPane, Saw, LCPane],
  [PWood, LCPane, PWood]]);

  recipes.remove(<StevesCarts:ModuleComponents:34>);
  recipes.addShaped(<StevesCarts:ModuleComponents:34>*4, [
  [CPane, CPane , CPane],
  [CPane, PIron, CPane],
  [CPane, Hammer, CPane]]);

  recipes.remove(<StevesCarts:ModuleComponents:35>);
  recipes.addShaped(<StevesCarts:ModuleComponents:35>, [
  [PIron, IPane, PIron],
  [IPane, Hammer, IPane],
  [PIron, IPane, PIron]]);

  recipes.remove(<StevesCarts:ModuleComponents:36>);
  recipes.addShaped(<StevesCarts:ModuleComponents:36>, [
  [PIron, LIPane, PIron],
  [LIPane, Hammer, LIPane],
  [PIron, LIPane, PIron]]);

  recipes.remove(<StevesCarts:ModuleComponents:37>);
  recipes.addShaped(<StevesCarts:ModuleComponents:37>*4 , [
  [IScrew, IPane, IScrew],
  [IPane, PRedAlloy, IPane],
  [IScrew, IPane, IScrew]]);

  recipes.remove(<StevesCarts:ModuleComponents:38>);
  recipes.addShaped(<StevesCarts:ModuleComponents:38>*2, [
  [DPane, DPane, DPane],
  [DPane, PRedAlloy, DPane],
  [DPane, DPane ,DPane]]);

  recipes.remove(<StevesCarts:ModuleComponents:39>);
  recipes.addShaped(<StevesCarts:ModuleComponents:39>, [
  [LDPane, LDPane, LDPane],
  [LDPane, Pcb, LDPane],
  [LDPane, Hammer,LDPane]]);

  recipes.remove(<StevesCarts:ModuleComponents:33>);
  recipes.addShapeless(<StevesCarts:ModuleComponents:33>*2 ,
  [PIron, Stone]);

  recipes.remove(<StevesCarts:ModuleComponents:61>);
  recipes.addShaped(<StevesCarts:ModuleComponents:61>*16 ,
  [
    [<ore:glass>, <ore:glass>, <ore:glass>],
    [<gregtech:gt.metaitem.03:32012>, <ore:glass>, <gregtech:gt.metaitem.03:32012>],
    [<ore:glass>, <ore:glass>, <ore:glass>],
  ]);

  recipes.remove(<StevesCarts:ModuleComponents:62>);

recipes.remove(<StevesCarts:ModuleComponents:62>);
recipes.addShaped(<StevesCarts:ModuleComponents:62>*2 ,
[
  [<StevesCarts:ModuleComponents:61>,<StevesCarts:ModuleComponents:61>,<StevesCarts:ModuleComponents:61>],
  [<StevesCarts:ModuleComponents:61>,SHammer, <StevesCarts:ModuleComponents:61>],
  [<StevesCarts:ModuleComponents:61>,<StevesCarts:ModuleComponents:61>,<StevesCarts:ModuleComponents:61>],
]);

recipes.remove(<StevesCarts:ModuleComponents:63>);
recipes.addShaped(<StevesCarts:ModuleComponents:63>,
[
  [<minecraft:glass_pane>, <StevesCarts:ModuleComponents:62>, <minecraft:glass_pane>],
  [<StevesCarts:ModuleComponents:62>, SHammer, <StevesCarts:ModuleComponents:62>],
  [<minecraft:glass_pane>, <StevesCarts:ModuleComponents:62>, <minecraft:glass_pane>],
]);

////////////////////////////////////////////
# Machine Recipes
////////////////////////////////////////////


//Advanced Dedector Rail
recipes.removeShaped(<StevesCarts:BlockAdvDetector>);
Assembler.addRecipe(<StevesCarts:BlockAdvDetector>*4, [<Railcraft:part.tie>*4, <Railcraft:part.rail>*6, <ore:plateRedAlloy>], null, 320, 16);


//Junction Rail
recipes.remove(<StevesCarts:BlockJunction>);
Assembler.addRecipe(<StevesCarts:BlockJunction>, <minecraft:rail>, <ore:plateRedAlloy>*4, 320, 16);

//Metal Blocks
recipes.remove(<StevesCarts:BlockMetalStorage>);
mods.gregtech.ImplosionCompressor.addRecipe(<StevesCarts:BlockMetalStorage>, <StevesCarts:ModuleComponents:22>* 9, 64);

recipes.remove(<StevesCarts:BlockMetalStorage:1>);
mods.gregtech.ImplosionCompressor.addRecipe(<StevesCarts:BlockMetalStorage:1>, <StevesCarts:ModuleComponents:47>*9, 64);

recipes.remove(<StevesCarts:BlockMetalStorage:2>);
mods.gregtech.ImplosionCompressor.addRecipe(<StevesCarts:BlockMetalStorage:2>, <StevesCarts:ModuleComponents:49>*9, 64);

//Reinforced and Galgadorian Metals

recipes.remove(<StevesCarts:ModuleComponents:22>);
furnace.remove(<StevesCarts:ModuleComponents:22>);
mods.gregtech.BlastFurnace.addRecipe([<StevesCarts:ModuleComponents:22>], <liquid:oxygen> * 1000, [<StevesCarts:ModuleComponents:21>], 1000, 120, 1500);
mods.gregtech.ForgeHammer.addRecipe(<StevesCarts:ModuleComponents:22>*9, <StevesCarts:BlockMetalStorage>, 200, 32);

recipes.remove(<StevesCarts:ModuleComponents:47>);
furnace.remove(<StevesCarts:ModuleComponents:47>);
mods.gregtech.BlastFurnace.addRecipe([<StevesCarts:ModuleComponents:47>], <liquid:oxygen>*1000, [<StevesCarts:ModuleComponents:46>], 2000, 120, 1800);
mods.gregtech.ForgeHammer.addRecipe(<StevesCarts:ModuleComponents:47>*9, <StevesCarts:BlockMetalStorage:1>, 600, 32);

recipes.remove(<StevesCarts:ModuleComponents:49>);
furnace.remove(<StevesCarts:ModuleComponents:49>);
recipes.remove(<StevesCarts:ModuleComponents:48>);
mods.gregtech.ImplosionCompressor.addRecipe(<StevesCarts:ModuleComponents:49>, <StevesCarts:ModuleComponents:48>, 64);
mods.gregtech.ImplosionCompressor.addRecipe(<StevesCarts:ModuleComponents:48>, <StevesCarts:ModuleComponents:46>*9, 64);
mods.gregtech.ForgeHammer.addRecipe(<StevesCarts:ModuleComponents:49>*9 ,<StevesCarts:BlockMetalStorage:2>,1200, 64);

recipes.remove(<StevesCarts:ModuleComponents:18>);
furnace.remove(<StevesCarts:ModuleComponents:19>);
mods.gregtech.Mixer.addRecipe(<StevesCarts:ModuleComponents:18>, null, [<ore:dustObsidian>*2, <ore:dustDiamond>], null, 900, 80);
recipes.remove(<StevesCarts:ModuleComponents:19>);
mods.gregtech.ImplosionCompressor.addRecipe(<StevesCarts:ModuleComponents:19>, <StevesCarts:ModuleComponents:18>, 32);

recipes.remove(<StevesCarts:ModuleComponents:20>);
mods.gregtech.AlloySmelter.addRecipe(<StevesCarts:ModuleComponents:20>, <StevesCarts:ModuleComponents:19>*4, <minecraft:iron_bars>*5, 400, 33);

recipes.remove(<StevesCarts:ModuleComponents:21>);
recipes.addShaped(<StevesCarts:ModuleComponents:21>,
[
  [<ore:ingotSteel>, <StevesCarts:ModuleComponents:20>, <ore:ingotSteel>],
  [<ore:ingotSteel>, <ore:ingotSteel>, <ore:ingotSteel>],
  [<StevesCarts:ModuleComponents:19>, <StevesCarts:ModuleComponents:19>, <StevesCarts:ModuleComponents:19>],
]);

recipes.remove(<StevesCarts:ModuleComponents:45>);
mods.gregtech.Mixer.addRecipe(<StevesCarts:ModuleComponents:45>, null, [<minecraft:ender_eye>, <minecraft:ghast_tear>*2, <minecraft:fermented_spider_eye>*2, <minecraft:magma_cream>*2], null, 1200, 60);

recipes.remove(<StevesCarts:CartModule:43>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:43>, [<StevesCarts:BlockMetalStorage>, <StevesCarts:ModuleComponents:22>*3, <StevesCarts:CartModule:8>, <StevesCarts:ModuleComponents:20>*2], null, 2000, 80);

recipes.remove(<StevesCarts:CartModule:39>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:39>, <StevesCarts:ModuleComponents:23>*2, <StevesCarts:ModuleComponents:22>*5, null, 2000, 80);

recipes.remove(<StevesCarts:ModuleComponents:80>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:ModuleComponents:80>, [<ore:plateSteel>*4, <StevesCarts:ModuleComponents:22>*2, <StevesCarts:ModuleComponents:15>*2, <gregtech:gt.metaitem.01:32721>], null, 900, 50);

recipes.remove(<StevesCarts:CartModule:79>.withTag({Data: 100 as byte}));
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:79>, [<StevesCarts:ModuleComponents:80>*5, <StevesCarts:ModuleComponents:17>, <gregtech:gt.metaitem.01:32601>], null, 2000, 80);

recipes.remove(<StevesCarts:CartModule:56>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:56>, [<gregtech:gt.blockcasings:2>, <gregtech:gt.metaitem.01:32641>*2, <gregtech:gt.metaitem.01:32601>, <StevesCarts:ModuleComponents:16>*2, <StevesCarts:ModuleComponents:58>*2, <gregtech:gt.metaitem.01:32750>], null, 4000, 75);

recipes.remove(<StevesCarts:ModuleComponents:58>);
recipes.addShaped(<StevesCarts:ModuleComponents:58>,
[
  [<StevesCarts:ModuleComponents:44>, <IC2:blockAlloyGlass>, <StevesCarts:ModuleComponents:44>],
  [A_Pcb, <gregtech:gt.blockmachines:4115>, A_Pcb],
  [Hammer, <gregtech:gt.metaitem.01:32601>, Wrench],
]);

recipes.remove(<StevesCarts:ModuleComponents:44>);
recipes.addShaped(<StevesCarts:ModuleComponents:44>,
[
  [<IC2:blockAlloyGlass>, <IC2:blockAlloyGlass>, <IC2:blockAlloyGlass>],
  [<minecraft:glowstone>, <gregtech:gt.blockmachines:4401>, <minecraft:glowstone>],
  [<gregtech:gt.metaitem.01:18054>, <gregtech:gt.metaitem.01:32600>, <gregtech:gt.metaitem.01:18054>],
]);

recipes.remove(<StevesCarts:CartModule:49>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:49>, [<StevesCarts:ModuleComponents:22>*4, <StevesCarts:ModuleComponents:16>*3, <minecraft:end_stone>, <StevesCarts:ModuleComponents:45>], null, 1200, 75);

recipes.remove(<StevesCarts:ModuleComponents:46>);
mods.gregtech.Mixer.addRecipe(<StevesCarts:ModuleComponents:46>, null, [<gregtech:gt.metaitem.02:29500>, <StevesCarts:ModuleComponents:45>*3, <StevesCarts:ModuleComponents:21>*4, <minecraft:dye:5>], null, 2400, 120);

recipes.remove(<StevesCarts:CartModule:9>.withTag({Data: 100 as byte}));
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:9>.withTag({Data: 100 as byte}), [<gregtech:gt.metaitem.01:32601> ,<StevesCarts:BlockMetalStorage>, <StevesCarts:CartModule:43>.withTag({Data: 100 as byte}), <StevesCarts:ModuleComponents:49>, <StevesCarts:ModuleComponents:47>*4,], null, 2400, 120);

recipes.remove(<StevesCarts:ModuleComponents:82>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:ModuleComponents:82>, [<StevesCarts:BlockMetalStorage>, <StevesCarts:ModuleComponents:47>*4], null, 1200, 60);

recipes.remove(<StevesCarts:CartModule:81>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:81>, [<StevesCarts:ModuleComponents:49>, <StevesCarts:ModuleComponents:47>*4, <StevesCarts:ModuleComponents:82>*2, <StevesCarts:CartModule:39>], null, 3600, 120);

recipes.remove(<StevesCarts:CartModule:80>.withTag({Data: 100 as byte}));
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:80>.withTag({Data: 100 as byte}), [<gregtech:gt.metaitem.01:32601>, <StevesCarts:ModuleComponents:47>*2, <StevesCarts:ModuleComponents:81>*5, <StevesCarts:CartModule:79>.withTag({Data: 100 as byte}), <StevesCarts:ModuleComponents:17>*2], null, 2000, 100);

recipes.remove(<StevesCarts:CartModule:84>.withTag({Data: 100 as byte}));
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:84>.withTag({Data: 100 as byte}), [<StevesCarts:CartModule:14>.withTag({Data: 100 as byte}), <StevesCarts:ModuleComponents:16>*2, <StevesCarts:ModuleComponents:47>*5, <gregtech:gt.metaitem.01:32601>], null, 2200, 100);

recipes.remove(<StevesCarts:CartModule:70>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:70>, [<minecraft:nether_brick>*3, <StevesCarts:ModuleComponents:22>*4, <gregtech:gt.metaitem.01:32641>, <gregtech:gt.blockmachines:102>], null, 1200, 60);

recipes.remove(<StevesCarts:CartModule:1>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:1>, [<StevesCarts:ModuleComponents:58>, <gregtech:gt.metaitem.01:32750>, <gregtech:gt.blockcasings:1>, <StevesCarts:ModuleComponents:16>, <gregtech:gt.metaitem.01:32601>, <gregtech:gt.metaitem.01:32641>], null, 3000, 70);

recipes.remove(<StevesCarts:CartModule:45>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:45>, [<gregtech:gt.metaitem.01:32750>, <StevesCarts:ModuleComponents:44>*4, <gregtech:gt.metaitem.01:32600>, <gregtech:gt.metaitem.01:32640>*2, <StevesCarts:ModuleComponents:16>*2, <gregtech:gt.blockcasings:1>],null, 3000, 65);

recipes.remove(<StevesCarts:upgrade:11>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:upgrade:11>, [<Railcraft:anvil>, <StevesCarts:ModuleComponents:16>*2, <gregtech:gt.metaitem.01:17019>*4, <StevesCarts:ModuleComponents:59>], null, 1000, 60);

recipes.remove(<StevesCarts:upgrade:12>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:upgrade:12>, [<minecraft:anvil>, <StevesCarts:ModuleComponents:16>*2, <gregtech:gt.metaitem.01:17019>*4, <StevesCarts:ModuleComponents:59>,<StevesCarts:BlockAdvDetector>], null, 1000, 60);

recipes.remove(<StevesCarts:CartModule:83>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:83>, [<StevesCarts:ModuleComponents:45>, <StevesCarts:ModuleComponents:16>*2, <minecraft:quartz>*3, <StevesCarts:ModuleComponents:47>*2, <minecraft:redstone_torch>], null, 1600, 80);

recipes.remove(<StevesCarts:CartModule:82>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:82>, [<ore:bookEnchanted>,<minecraft:enchanting_table>,<StevesCarts:ModuleComponents:16>*2,<StevesCarts:ModuleComponents:47>*2],null,2000,80);

recipes.remove(<StevesCarts:CartModule:32>);
mods.gregtech.Assembler.addRecipe(<StevesCarts:CartModule:32>, [<minecraft:obsidian>*2, <StevesCarts:ModuleComponents:19>*4, <minecraft:diamond_block>],null,600,50);
