//tank

recipes.remove(<EnderStorage:enderChest:4096>);

recipes.addShaped(<EnderStorage:enderChest:4096>,
  [
    [<ore:stickLongBlaze>, <minecraft:wool>, <ore:stickLongBlaze>],
    [<ore:plateDenseObsidian>, <minecraft:cauldron>, <ore:plateDenseObsidian>],
    [<ore:stickLongBlaze>, <gregtech:gt.metaitem.01:32762>, <ore:stickLongBlaze>]
  ]
);

//chest

recipes.remove(<EnderStorage:enderChest>);

recipes.addShaped(<EnderStorage:enderChest>,
  [
    [<ore:stickLongBlaze>, <minecraft:wool>, <ore:stickLongBlaze>],
    [<ore:plateDenseObsidian>, <minecraft:chest:*>, <ore:plateDenseObsidian>],
    [<ore:stickLongBlaze>, <gregtech:gt.metaitem.01:32762>, <ore:stickLongBlaze>]
  ]
);
