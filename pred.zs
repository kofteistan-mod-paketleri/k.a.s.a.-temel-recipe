import mods.gregtech.Compressor;
import mods.gregtech.AlloySmelter;
import mods.gregtech.Assembler;

//silicone chip

recipes.remove(<ProjRed|Core:projectred.core.part:7>);

Assembler.addRecipe(<ProjRed|Core:projectred.core.part:7>, [<ProjRed|Core:projectred.core.part>*3,<ProjRed|Core:projectred.core.part:13>], null, 40, 26);

//energized silicone chip

recipes.remove(<ProjRed|Core:projectred.core.part:8>);

Assembler.addRecipe(<ProjRed|Core:projectred.core.part:8>, [<ProjRed|Core:projectred.core.part>*3,<ProjRed|Core:projectred.core.part:14>], null, 40, 26);

//screwdriver

recipes.remove(<ProjRed|Core:projectred.core.screwdriver>);

recipes.addShaped(<ProjRed|Core:projectred.core.screwdriver>,
  [
    [    <ore:plateIron>     ,  <ore:craftingToolHardHammer>  ,      null      ],
    [<ore:craftingToolFile>  ,         <ore:plateIron>        ,  <ore:dyeBlue> ],
    [         null           ,          <ore:dyeBlue>         , <ore:plateIron>]
  ]
);


//eleectro silicon

furnace.remove(<ProjRed|Core:projectred.core.part:59>);

AlloySmelter.addRecipe(<ProjRed|Core:projectred.core.part:59>,<ProjRed|Core:projectred.core.part:12>,<ore:dustElectrotine>*8,80,28);

//electrotine silicon compound

recipes.remove(<ProjRed|Core:projectred.core.part:58>);

//energized silicon

furnace.remove(<ProjRed|Core:projectred.core.part:14>);

AlloySmelter.addRecipe(<ProjRed|Core:projectred.core.part:14>,<ProjRed|Core:projectred.core.part:12>,<minecraft:glowstone_dust>*8,80,28);

//glowing silicon compound

recipes.remove(<ProjRed|Core:projectred.core.part:43>);

//infused silicon

furnace.remove(<ProjRed|Core:projectred.core.part:13>);

AlloySmelter.addRecipe(<ProjRed|Core:projectred.core.part:13>,<ProjRed|Core:projectred.core.part:12>,<minecraft:redstone>*8,80,28);

//red silicon compound

recipes.remove(<ProjRed|Core:projectred.core.part:42>);

//electrotine alloy  ingot

furnace.remove(<ProjRed|Core:projectred.core.part:55>);

AlloySmelter.addRecipe(<ProjRed|Core:projectred.core.part:55>,<ore:ingotIron>,<ore:dustElectrotine>*8,80,28);

//electrotine alloy compound

recipes.remove(<ProjRed|Core:projectred.core.part:57>);

//red alloy

furnace.remove(<gregtech:gt.metaitem.01:11308>,<ProjRed|Core:projectred.core.part:40>);

//red iron compound

recipes.remove(<ProjRed|Core:projectred.core.part:40>);

//sandy coal compound

recipes.remove(<ProjRed|Core:projectred.core.part:41>);

//silicon boule

furnace.remove(<ProjRed|Core:projectred.core.part:11>);

//circuit plate

furnace.remove(<ProjRed|Core:projectred.core.part>);

Compressor.addRecipe(<ProjRed|Core:projectred.core.part>,<ore:dustStone>*2,80,30);

//red alloy wire

recipes.remove(<ProjRed|Transmission:projectred.transmission.wire>);

recipes.addShapeless(<ProjRed|Transmission:projectred.transmission.wire>,[<ore:wireGt01RedAlloy>]);

//insulated

for i in 0 to 16{
  val wire = <ProjRed|Transmission:projectred.transmission.wire>.definition.makeStack(i+1);
  val wool = <minecraft:carpet:0>.definition.makeStack(i);
  recipes.remove(wire);
  recipes.addShapeless(wire,[<ProjRed|Transmission:projectred.transmission.wire>,wool]);
}

//silicon

recipes.remove(<ProjRed|Core:projectred.core.part:12>);

AlloySmelter.addRecipe(<ProjRed|Core:projectred.core.part:12>,<gregtech:gt.metaitem.01:32307>*0,<ore:dustSilicon>*4,160,30);
