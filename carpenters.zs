import mods.gregtech.CuttingSaw;

//hammer

recipes.remove(<CarpentersBlocks:itemCarpentersHammer>);

recipes.addShaped(<CarpentersBlocks:itemCarpentersHammer>,
  [
    [<ore:plateIron>,               <ore:plateIron>            , <ore:craftingToolHardHammer>],
    [      null     , <CarpentersBlocks:blockCarpentersBlock:*>,       <ore:plateIron>       ],
    [      null     , <CarpentersBlocks:blockCarpentersBlock:*>,              null           ]
  ]
);

//chisel

recipes.remove(<CarpentersBlocks:itemCarpentersChisel>);

recipes.addShaped(<CarpentersBlocks:itemCarpentersChisel>,
  [
    [       <ore:craftingToolHardHammer>      ],
    [              <ore:plateIron>            ],
    [<CarpentersBlocks:blockCarpentersBlock:*>]
  ]
);

//carpenters block

recipes.remove(<CarpentersBlocks:blockCarpentersBlock>);

recipes.addShaped(<CarpentersBlocks:blockCarpentersBlock>*4,
  [
    [   <ore:stickWood>   , <ore:stickWood> ,     <ore:stickWood>    ],
    [<ore:craftingToolSaw>,<ore:frameGtWood>,<ore:craftingToolWrench>],
    [   <ore:stickWood>   , <ore:stickWood> ,     <ore:stickWood>    ]
  ]
);

//wedge slope

recipes.remove(<CarpentersBlocks:blockCarpentersSlope>);

recipes.addShaped(<CarpentersBlocks:blockCarpentersSlope>*2,
  [
    [<CarpentersBlocks:blockCarpentersBlock>,<ore:craftingToolSaw>]
  ]
);

CuttingSaw.addRecipe([<CarpentersBlocks:blockCarpentersSlope>*4], <CarpentersBlocks:blockCarpentersBlock>, <liquid:water> * 5, 160, 8);

CuttingSaw.addRecipe([<CarpentersBlocks:blockCarpentersSlope>*4], <CarpentersBlocks:blockCarpentersBlock>, <liquid:ic2distilledwater> * 3, 160, 8);

CuttingSaw.addRecipe([<CarpentersBlocks:blockCarpentersSlope>*4], <CarpentersBlocks:blockCarpentersBlock>, <liquid:lubricant> * 1, 80, 8);
