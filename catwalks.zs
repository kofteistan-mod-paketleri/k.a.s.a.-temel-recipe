//blowtorch

recipes.removeShaped(<catwalks:blowtorch>, [[<minecraft:flint_and_steel>, null, null], [null, <ore:ingotIron>, null], [null, null, <ore:ingotIron>]]);

recipes.addShaped(<catwalks:blowtorch>,
  [
    [<minecraft:flint_and_steel>, null, null],
    [null, <ore:stickIron>, <ore:screwIron>],
    [<ore:screwIron>, <ore:craftingToolScrewdriver>, <ore:stickIron>]
  ]
);

//ropelight

recipes.remove(<catwalks:ropeLight>);

recipes.addShaped(<catwalks:ropeLight>*8,
  [
    [<ore:dustGlowstone>,<minecraft:string>,<ore:dustGlowstone>],
    [null,<ore:craftingToolWireCutter>,null]
  ]
);

//steel grate

recipes.removeShaped(<catwalks:steelgrate>, [[<ore:ingotIron>, null, <ore:ingotIron>], [null, <ore:ingotIron>, null], [<ore:ingotIron>, null, <ore:ingotIron>]]);

recipes.addShaped(<catwalks:steelgrate>*4,
  [
    [<ore:screwSteel>, <ore:stickSteel>, <ore:screwSteel>],
    [<ore:stickSteel>,  <ore:craftingToolScrewdriver>, <ore:stickSteel>],
    [<ore:screwSteel>, <ore:stickSteel>, <ore:screwSteel>]
  ]
);
